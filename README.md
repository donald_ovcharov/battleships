# README #

![ship-blueprint.png](https://bitbucket.org/repo/aX4ppp/images/3951725413-ship-blueprint.png)

### What is this repository for? ###

* BattleShips game task.

### How do I get set up? ###

* clone git project
* php -S 0.0.0.0:8002 for web interface
* php index.php for cli interface
