<?php
namespace BattleShips\GameEngine;

use BattleShips\Board\Presenters\WebBoardPresenter;

class WebGameEngine extends BaseGameEngine{

    private $is_started = false;

    public function __construct(){
        session_start();
        $this->presenter = new WebBoardPresenter();
    }

    public function run()
    {
        if(!$this->isStarted()){
            $this->start();
            $this->init();
            $this->renderUserInterface(new Response(Response::CODE_START_GAME));
            $this->save();
        }else{
            $this->load();
            $request = $this->getUserRequest();
            $response = $this->processUserRequest($request);
            $this->renderUserInterface($response);
            $this->save();
        }
    }

    private function save(){
        $_SESSION['Board'] = $this->board;
        $_SESSION['presenter'] = $this->presenter;
        $_SESSION['user_actions'] = $this->user_actions;
        $_SESSION['user_ship_hits'] = $this->user_ship_hits;
    }

    private function load(){
        $this->board = $_SESSION['Board'];
        $this->presenter = $_SESSION['presenter'];
        $this->user_actions = $_SESSION['user_actions'];
        $this->user_ship_hits = $_SESSION['user_ship_hits'];
    }

    private function isStarted()
    {
        return !empty($_SESSION['started']);
    }
    private function start(){
        $_SESSION['started'] = 1;
    }

    public function getUserRequest()
    {
        return isset($_POST['request']) ? $_POST['request'] : false;
    }

    public function renderUserInterface($response)
    {
        $board_draw = $this->presenter->draw($this->board);
        $this->renderPage($response,$board_draw);

    }

    private function renderPage($response,$board_draw){
        require_once(APP_PATH.
            DIRECTORY_SEPARATOR."BattleShips".
            DIRECTORY_SEPARATOR."Views".
            DIRECTORY_SEPARATOR."web.index.php");
    }

    public function over()
    {
        session_destroy();
    }

}