<?php

namespace BattleShips\GameEngine;

use BattleShips\Board\Board;

class GameEngineFactory {

    const ENV_CLI = 1;
    const ENV_WEB = 2;

    private static $instance = null;

    private $environment;
    private $GameEngine;

    private function __construct(){
        $this->considerEnvironment();
    }

    /**
     * @return GameEngineFactory
     */
    public static function getInstance(){
        if(!self::$instance){
            self::$instance = new GameEngineFactory();
        }
        return self::$instance;
    }

    public function getGameEngine(){
        if($this->isCLI()){
            $this->GameEngine = $this->getCliGameEngine();
        }

        if($this->isWEB()){
            $this->GameEngine = $this->getWebGameEngine();
        }

        return $this->GameEngine;
    }

    private function getCliGameEngine(){
        return new CliGameEngine;
    }

    private function getWebGameEngine(){
        return new WebGameEngine;
    }

    private function isCLI(){
        return $this->environment == self::ENV_CLI;
    }

    private function isWEB(){
        return $this->environment == self::ENV_WEB;
    }

    private function considerEnvironment(){
        //Any missing cli condition should be added here.
        $cli_cond1 = php_sapi_name() === 'cli';
        $cli_cond2 = defined('STDIN');

        $this->environment = ($cli_cond1 || $cli_cond2) ? self::ENV_CLI : self::ENV_WEB;
    }
}