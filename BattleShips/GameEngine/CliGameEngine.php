<?php

namespace BattleShips\GameEngine;

use BattleShips\Board\Presenters\CliBoardPresenter;

class CliGameEngine extends BaseGameEngine
{

    public function run()
    {
        $this->init();
        $this->presenter = new CliBoardPresenter();
        $response = new Response(Response::CODE_START_GAME);
        $this->renderUserInterface($response);
        while($response->code != Response::CODE_GAME_OVER) {
            $request = $this->getUserRequest();
            $response = $this->processUserRequest($request);
            $this->renderUserInterface($response);
        }
    }

    public function getUserRequest()
    {
        echo "Choose target: ";
        $action = fgets(STDIN);
        return trim($action, "\n");
    }

    public function renderUserInterface($response)
    {
        echo $response."\n";
        $this->presenter->draw($this->board)->flush();
    }

    public function over()
    {
        //Do nothing.
    }
}