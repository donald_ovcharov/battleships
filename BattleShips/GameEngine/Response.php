<?php
namespace BattleShips\GameEngine;

class Response {

    const CODE_MISS = 1;
    const CODE_HIT = 2;
    const CODE_ALREADY_HIT = 3;
    const CODE_ALREADY_MISS = 4;
    const CODE_SUNK = 5;
    const CODE_GAME_OVER = 6;
    const CODE_START_GAME = 7;// used for web only
    const CODE_DEBUG = 8;
    const CODE_NONE = 9;
    const CODE_ERROR = 999;

    public $code;
    public $message;

    public function __construct($code){
        $this->code = $code;
        switch ($code) {
            case self::CODE_HIT:
            case self::CODE_ALREADY_HIT: {
                $this->message = "Hit!";break;
            }
            case self::CODE_MISS:
            case self::CODE_ALREADY_MISS: {
                $this->message = "Miss!";break;
            }
            case self::CODE_SUNK: {
                $this->message = "Sunk!";break;
            }
            case self::CODE_GAME_OVER: {
                $this->message = "Game Over.";break;
            }
            case self::CODE_START_GAME: {
                $this->message = "Enjoy your game!";break;
            }
            case self::CODE_ERROR: {
                $this->message = "Error.";break;
            }
            case self::CODE_NONE: {
                $this->message = "";break;
            }
            case self::CODE_DEBUG: {
                $this->message = "Hidden ships are displayed with `O`.";break;
            }
            default:{
                $this->message = "Error.";break;
            }
        }
    }

    public function __toString(){
        return $this->message;
    }
}