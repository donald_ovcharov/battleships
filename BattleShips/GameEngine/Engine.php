<?php

namespace BattleShips\GameEngine;

interface Engine {
    public function run();
    public function getUserRequest();
    public function processUserRequest($request);
    public function renderUserInterface($response);
    public function over();
}