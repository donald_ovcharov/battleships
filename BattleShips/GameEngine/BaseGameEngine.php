<?php

namespace BattleShips\GameEngine;

use BattleShips\Board\Block;
use BattleShips\Board\Board;
use BattleShips\Board\InvalidBlockValuesException;
use BattleShips\Ships\Ship;
use BattleShips\Ships\ShipFactory;

abstract class BaseGameEngine implements Engine
{
    protected $presenter;

    const SHIPS_CONFIG = [
        Ship::LEVEL_BATTLESHIP => 1,
        SHip::LEVEL_DESTROYER => 2
    ];

    /** @var  Board */
    protected $board;

    /** @var  ShipFactory */
    protected $shipFactory;

    protected $user_actions = 0;
    protected $user_ship_hits = 0;

    public function init()
    {
        $this->board = new Board();
        $this->shipFactory = new ShipFactory($this->board);
        $ships = $this->shipFactory->getShips(self::SHIPS_CONFIG);
        $this->board->placeShips($ships);
    }

    public function processUserRequest($request)
    {
        if(!$request){
            return new Response(Response::CODE_NONE);
        }
        if($request == "show"){
            $this->presenter->debug = true;
            return new Response(Response::CODE_DEBUG);
        }
        try {
            $this->user_actions++;

            list($x, $y) = Block::parseBlockXY($request);
            $block = $this->board->getBlock($x, $y);

            switch ($block->getState()) {
                case Block::STATE_FREE: {
                    $block->setState(Block::STATE_MISS);
                    return new Response(Response::CODE_MISS);
                }
                case Block::STATE_SHIP_HIDDEN: {
                    $block->setState(Block::STATE_SHIP_HIT);
                    $this->user_ship_hits++;
                    $ship = $block->getShip();

                    if($this->checkGameOver()){
                        $response = new Response(Response::CODE_GAME_OVER);
                        $response->message = "Well played. All ships are sunk in {$this->user_actions} shots.\n";
                        $this->over();
                    }elseif($ship->isSunk()) {
                        $response = new Response(Response::CODE_SUNK);
                    }else{
                        $response = new Response(Response::CODE_HIT);
                    }

                    return $response;
                }
                case Block::STATE_SHIP_HIT: {
                    return new Response(Response::CODE_ALREADY_HIT);
                }
                case Block::STATE_MISS: {
                    return new Response(Response::CODE_ALREADY_MISS);
                }
            }
        } catch (InvalidBlockValuesException $e) {
        }
        return new Response(Response::CODE_ERROR);
    }

    public function checkGameOver()
    {
        return $this->user_ship_hits == $this->board->getShipBlocksCount();
    }
}