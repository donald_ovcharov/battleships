<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>BattleShips</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-2 col-sm-2 hidden-xs"></div>
        <div class="col-lg-6 col-md-8 col-sm-8 col-xs-12">
            <form method="post" class="form">
                <fieldset>
                    <!-- Form Name -->
                    <legend>A game of Battle Ships</legend>
                    <!-- Button -->
                    <div class="control-group">
                        <div class="controls">
                            <label for="request">Enter coordinates (row, col), e.g. A5 </label>
                            <input tabindex="1" autofocus id="request" type="text" name="request"/>
                            <button name="submit" class="btn btn-info">Submit</button>
                        </div>
                    </div>

                    <div id="response">
                        <p class="text-info"><?php echo $response ?>
                            <?php if($response->code == \BattleShips\GameEngine\Response::CODE_GAME_OVER): ?>
                                <a href="/">Play Again.</a>
                            <?php endif;?>
                        </p>
                    </div>

                    <div class="form-group">
                        <?php $board_draw->flush();?>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="col-lg-3 col-md-2 col-sm-2 hidden-xs"></div>
    </div>
</div>
</body>
</html>