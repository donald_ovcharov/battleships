<?php

namespace BattleShips\Ships;

class ShipsCollection extends \ArrayObject{

    public function __construct($input = []){
        if($input){
            if(!is_array($input)){
                $input = [$input];
            }
            foreach($input as $obj){
                if(!$obj instanceof Ship){
                    throw new TypeNotAllowedException;
                }
            }
        }
        parent::__construct($input);
    }

    public function append(Ship $ship){
        parent::append($ship);
    }

}

class TypeNotAllowedException extends \Exception {

}