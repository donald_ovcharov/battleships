<?php

namespace BattleShips\Ships;

use BattleShips\Board\Block;

class ShipBlueprint {
    /**
     * @var Block
     */
    public $starting_block;

    public $direction;

    public $level;

    /**
     * @param Block $starting_block
     * @param $direction
     * @param $level
     */
    public function __construct(Block $starting_block,$direction,$level){
        $this->starting_block = $starting_block;
        $this->direction = $direction;
        $this->level = $level;
    }

    public function toArray(){
        return [
            'starting_block'=>$this->starting_block->toArray(),
            'direction'=>$this->direction,
            'level'=>$this->level
        ];
    }
}