<?php

namespace BattleShips\Ships;

use BattleShips\Board\Board;

class ShipFactory
{

    /**
     * @var Board
     */
    private $board;

    public function __construct(Board $board)
    {
        $this->board = $board;
    }

    public function getShips($ships_config)
    {
        $ships = new ShipsCollection();
        foreach ($ships_config as $level => $count) {
            for ($i = 0; $i < $count; $i++) {
                $ship = $this->getShip($level);
                $ships->append($ship);
            }
        }
        return $ships;
    }

    public function getShip($level)
    {
        while (true) {
            try {
                $blueprint = $this->getRandomBlueprint($level);
                return $this->constructShip($blueprint);
            } catch (ShipOverlapException $e) {
                //continue loop
            }
        }
    }

    private function getRandomBlueprint($level)
    {
        $directions = ['up', 'left', 'right', 'down'];
        $rand_direction = $directions[rand(0, 3)];

        $min_x = Board::X_RANGE_MIN;
        $max_x = Board::X_RANGE_MAX;
        $min_y = Board::Y_RANGE_MIN;
        $max_y = Board::Y_RANGE_MAX;

        // ensure not out of bound ships
        switch ($rand_direction) {
            case "left": {
                $min_y = $min_y + $level;break;
            }
            case "right": {
                $max_y = $max_y - $level;break;
            }
            case "up": {
                $min_x = chr(ord($min_x) + $level);break;
            }
            case "down": {
                $max_x = chr(ord($max_x) - $level);break;
            }
        }
        $rand_x = chr(rand(ord($min_x), ord($max_x)));
        $rand_y = rand($min_y, $max_y);

        $starting_block = $this->board->getBlock($rand_x, $rand_y);
        return new ShipBlueprint($starting_block, $rand_direction, $level);
    }

    public function constructShip(ShipBlueprint $blueprint)
    {
        $blocks = [];
        $block = null;
        for ($i = 0; $i < $blueprint->level; $i++) {
            if ($i == 0) {
                $block = $blueprint->starting_block;
            } else {
                $block = $block->{$blueprint->direction}();
            }
            $blocks[] = $block;
            if ($block->getShip()) {
                throw new ShipOverlapException();
            }
        }
        $ship = new Ship($blocks);
        $ship->setBlueprint($blueprint);
        return $ship;
    }
}

class ShipOverlapException extends \Exception
{

}