<?php

namespace BattleShips\Ships;

use BattleShips\Board\Block;

class Ship
{

    const LEVEL_BATTLESHIP = 5;
    const LEVEL_DESTROYER = 4;

    private $blocks;
    private $blueprint;

    //@TODO blocks collection
    public function __construct($blocks)
    {
        if (!is_array($blocks)) {
            $blocks = [$blocks];
        }

        foreach ($blocks as $block) {
            if (!$block instanceof Block) {
                throw new TypeNotAllowedException;
            }
        }
        $this->blocks = $blocks;
        $this->connectBlocks();
    }

    private function connectBlocks()
    {
        /** @var Block $block */
        foreach ($this->blocks as $block) {
            $block->setShip($this);
        }
    }

    public function getBlocks()
    {
        return $this->blocks;
    }

    public function isSunk()
    {
        /** @var Block $block */
        foreach ($this->blocks as $block) {
            if ($block->getState() == Block::STATE_SHIP_HIDDEN) {
                return false;
            }
        }
        return true;
    }

    public function setBlueprint(ShipBlueprint $blueprint){
        $this->blueprint = $blueprint;
    }

    /**
     * @return ShipBlueprint
     */
    public function getBlueprint(){
        return $this->blueprint;
    }
}