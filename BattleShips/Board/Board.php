<?php

namespace BattleShips\Board;

use BattleShips\Ships\Ship;
use BattleShips\Ships\ShipBlueprint;
use BattleShips\Ships\ShipFactory;
use BattleShips\Ships\ShipsCollection;

class Board implements \Serializable
{
    const X_RANGE_MIN = 'A';
    const X_RANGE_MAX = 'J';
    const Y_RANGE_MIN = 1;
    const Y_RANGE_MAX = 10;
    /**
     * @var ShipsCollection
     */
    private $ships;

    /**
     * @var Block[]
     */
    private $blocks;

    private $ship_blocks_count = 0;

    /**
     * Note: modifying constructor will affect serialize method!
     */
    public function __construct()
    {
        $this->fillEmptyBoard();
        $this->ships = new ShipsCollection();
    }

    public function placeShips(ShipsCollection $ships){
        /** @var Ship $ship */
        foreach($ships as $ship){
            $ship_blocks = $ship->getBlocks();
            /** @var Block $block */
            foreach($ship_blocks as $block){
                $block->setState(Block::STATE_SHIP_HIDDEN);
                $this->ship_blocks_count++;
            }
            $this->ships->append($ship);
        }
    }

    /**
     * @param $x
     * @param $y
     * @return Block
     */
    public function getBlock($x,$y){
        //@TODO invalid,outofbounds exception
        return $this->blocks[$x][$y];
    }

    public function getBlocks(){
        return $this->blocks;
    }

    public function getShipBlocksCount(){
        return $this->ship_blocks_count;
    }

    private function fillEmptyBoard()
    {
        $this->blocks = [];
        $this->iterateBoard(function($x,$y){
            if(!isset($this->blocks[$x])){
                $this->blocks[$x] = [];
            }
            $this->blocks[$x][$y] = new Block($this,$x,$y);
        },false);
    }

    public function iterateBoard(\Closure $c,$return_block = true){
        for ($x = self::X_RANGE_MIN; $x <= self::X_RANGE_MAX; $x++) {
            for ($y = self::Y_RANGE_MIN; $y <= self::Y_RANGE_MAX; $y++) {
                if($return_block){
                    $c($this->getBlock($x,$y));
                }else{
                    $c($x,$y);
                }
            }
        }
    }

    public function serialize()
    {
        $data = [
            'blocks' => [],
            'ships_blueprints' => [],
        ];

        //Serialize blocks
        $this->iterateBoard(function(Block $block) use (&$data){
            $data['blocks'][$block->getX()][$block->getY()] = $block->toArray();
        });

        //Serialize ships
        /** @var Ship $ship */
        foreach($this->ships as $ship){
            $data['ships_blueprints'][] = $ship->getBlueprint()->toArray();
        }
        return json_encode($data);
    }

    public function unserialize($serialized)
    {
        $this->__construct();
        $data = json_decode($serialized,true);

        $shipFactory = new ShipFactory($this);
        $shipsCollection = new ShipsCollection();
        foreach($data['ships_blueprints'] as $bp){
            $x = $bp['starting_block']['x'];
            $y = $bp['starting_block']['y'];
            $starting_block = $this->getBlock($x,$y);
            $blueprint = new ShipBlueprint($starting_block,$bp['direction'],$bp['level']);
            $shipsCollection->append($shipFactory->constructShip($blueprint));
        }

        $this->placeShips($shipsCollection);

        $this->iterateBoard(function($x,$y) use ($data){
            /** @var Block $block */
            $block = $data['blocks'][$x][$y];
            $this->getBlock($x,$y)->setState($block['state']);
        },false);
    }
}