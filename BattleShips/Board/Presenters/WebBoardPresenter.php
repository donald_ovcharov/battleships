<?php
namespace BattleShips\Board\Presenters;

use BattleShips\Board\Block;
use BattleShips\Board\Board;

class WebBoardPresenter extends BoardPresenter{

    public function draw(Board $board){
        $this->output('<table class="table"><tr>');
        parent::draw($board);
        $this->output('</tr></table>');
        return $this;
    }

    protected function drawBlock(Block $block){
        switch($block->getState()){
            case Block::STATE_FREE:        $this->output('<td> . </td>');break;
            case Block::STATE_SHIP_HIDDEN: $this->output($this->debug ? '<td> O </td>' : '<td> . </td>');break;
            case Block::STATE_MISS:        $this->output('<td> - </td>');break;
            case Block::STATE_SHIP_HIT:    $this->output('<td> X </td>');break;
        }
    }

    protected function drawNewLine(){
        $this->output("</tr><tr>");
    }

    protected function drawRowLabel($x){
        $this->output("<td>$x</td>");
    }
    protected function drawColLabel($y){
        $this->output("<th>$y</th>");
    }
    protected function drawEmptyBlock(){
        $this->output("<th></th>");
    }
}