<?php
namespace BattleShips\Board\Presenters;

use BattleShips\Board\Block;

class CliBoardPresenter extends BoardPresenter{

    protected function drawBlock(Block $block){
        switch($block->getState()){
            case Block::STATE_FREE:        $this->output(' . ');break;
            case Block::STATE_SHIP_HIDDEN: $this->output($this->debug ? ' O ' : ' . ');break;
            case Block::STATE_MISS:        $this->output(' - ');break;
            case Block::STATE_SHIP_HIT:    $this->output(' X ');break;
        }
    }

    protected function drawNewLine(){
        $this->output("\n");
    }

    protected function drawRowLabel($x){
        $this->output(" $x ");
    }

    protected function drawColLabel($y){
        $this->output(" $y ");
    }

    protected function drawEmptyBlock(){
        $this->output("   ");
    }
}