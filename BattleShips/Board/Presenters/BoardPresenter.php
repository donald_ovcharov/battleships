<?php

namespace BattleShips\Board\Presenters;

use BattleShips\Board\Block;
use BattleShips\Board\Board;

abstract class BoardPresenter{

    /**
     * @var Board
     */
    protected $board;

    protected $output;

    public $debug;

    public function __construct($debug = false){
        $this->debug = $debug;
    }

    /**
     * @param Board $board
     * @return $this
     */
    public function draw(Board $board){
        $this->board = $board;
        $this->drawHeader();
        $this->board->iterateBoard(function(Block $block){
            if($this->firstYElement($block)){
                $this->drawRowLabel($block->getX());
            }

            $this->drawBlock($block);

            if($this->lastYElement($block)){
                $this->drawNewLine();
            }
        });
        return $this;
    }

    private function lastYElement(Block $block){
        return $block->getY() == Board::Y_RANGE_MAX;
    }

    private function firstYElement(Block $block){
        return $block->getY() == Board::Y_RANGE_MIN;
    }

    public function flush(){
        echo $this->output;
        $this->output = "";
        if($this->debug){
            $this->debug = false;
        }
    }

    protected function output($str){
        $this->output .= $str;
    }

    protected function drawHeader(){
        $this->drawEmptyBlock();
        for($y = Board::Y_RANGE_MIN; $y <= Board::Y_RANGE_MAX;$y++){
            $this->drawColLabel($y);
        }
        $this->drawNewLine();
    }

    protected abstract function drawBlock(Block $block);
    protected abstract function drawNewLine();
    protected abstract function drawRowLabel($x);
    protected abstract function drawColLabel($y);
    protected abstract function drawEmptyBlock();
}