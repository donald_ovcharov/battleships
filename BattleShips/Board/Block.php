<?php

namespace BattleShips\Board;
use BattleShips\Ships\Ship;

/**
 * Class Block
 * @package BattleShips\Board
 */
class Block
{
    const STATE_FREE = 0;
    const STATE_MISS = 1;
    const STATE_SHIP_HIDDEN = 2;
    const STATE_SHIP_HIT = 3;

    private $board;

    private $x;
    private $y;
    private $state;

    /** @var Ship $ship */
    private $ship = null;

    /**
     * @param Board $board
     * @param $x
     * @param $y
     * @param int $state
     * @throws InvalidBlockValuesException
     */
    public function __construct(Board $board, $x, $y, $state = self::STATE_FREE)
    {
        $this->board = $board;
        $this->setX($x);
        $this->setY($y);
        $this->setState($state);
    }

    /**
     * @return Block
     */
    public function up()
    {
        $up_x = chr(ord($this->x) - 1);// ++ works but -- doesn't ???
        $block = $this->board->getBlock($up_x, $this->y);
        return $block;
    }

    /**
     * @return Block
     */
    public function down()
    {
        $down_x = $this->x;
        $down_x++;
        $block = $this->board->getBlock($down_x, $this->y);
        return $block;
    }

    /**
     * @return Block
     */
    public function left()
    {
        $left_y = $this->y;
        $left_y--;
        $block = $this->board->getBlock($this->x, $left_y);
        return $block;
    }

    /**
     * @return Block
     */
    public function right()
    {
        $right_y = $this->y;
        $right_y++;
        $block = $this->board->getBlock($this->x, $right_y);
        return $block;
    }

    /**
     * @param $state
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param $x
     * @return $this
     * @throws InvalidBlockValuesException
     */
    public function setX($x)
    {
        if (!$this->validateX($x)) {
            throw new InvalidBlockValuesException;
        }

        $this->x = $x;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param $y
     * @return $this
     * @throws InvalidBlockValuesException
     */
    public function setY($y)
    {
        if (!$this->validateY($y)) {
            throw new InvalidBlockValuesException;
        }

        $this->y = $y;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param \BattleShips\Ships\Ship $ship
     */
    public function setShip(Ship $ship)
    {
        $this->ship = $ship;
    }

    /**
     * @return \BattleShips\Ships\Ship
     */
    public function getShip()
    {
        return $this->ship;
    }

    /**
     * @param $xy
     * @return array
     * @throws InvalidBlockValuesException
     */
    public static function parseBlockXY($xy)
    {
        $xy = strtoupper($xy);
        $x = substr($xy, 0, 1);
        $y = substr($xy, 1);
        if (!(self::validateX($x) && self::validateY($y))) {
            throw new InvalidBlockValuesException;
        }
        return [$x, $y];
    }

    /**
     * @param $x
     * @return bool
     */
    private static function validateX($x)
    {
        $allowed_x_values = range(Board::X_RANGE_MIN, Board::X_RANGE_MAX);
        return in_array($x, $allowed_x_values);
    }

    /**
     * @param $y
     * @return bool
     */
    private static function validateY($y)
    {
        $allowed_y_values = range(Board::Y_RANGE_MIN, Board::Y_RANGE_MAX);
        return in_array($y, $allowed_y_values);
    }

    public function toArray(){
        return [
            'x'=>$this->x,
            'y'=>$this->y,
            'state'=>$this->state
        ];
    }
}

/**
 * Class InvalidBlockValuesException
 * @package BattleShips\Board
 */
class InvalidBlockValuesException extends \Exception
{

}