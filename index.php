<?php
require_once('autoload.php');

define('APP_PATH',__DIR__);

$GameEngineFactory = \BattleShips\GameEngine\GameEngineFactory::getInstance();
$GameEngine = $GameEngineFactory->getGameEngine();
$GameEngine->run();